package com.example.aditya.webservice;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Hero> datalist;

    public MyAdapter(List<Hero> datalist){
        this.datalist = datalist;
    }


    // this will append the list_view on activiy_main
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listitem = layoutInflater.inflate(R.layout.list_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(listitem);
        return viewHolder;
    }


    //this is to get the string data from datalist
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.names.setText(datalist.get(position).getName());
        viewHolder.realname.setText(datalist.get(position).getRealname());

    }


    // this will post the data on the specific view parts
    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        TextView names;
        TextView realname;
        public ViewHolder(@NonNull View itemView) {

            super(itemView);
            mView = itemView;
            names = mView.findViewById(R.id.textView);
            realname = mView.findViewById(R.id.textView2);
        }
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }
}
